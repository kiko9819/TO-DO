# TO-DO
  Hello and welcome to my little TO-DO list project.
This shall consist of simple implementation of how 
TO-DO's actually work.
  Using JS(and some libraries) and HTML&CSS, I will be
developing this TO-DO list.
  The user will be able to choose between his projects.
By projects I mean separate TO-DO lists. Also, the user
will be able to change titles, due-dates, he will be able
to prioritize his TO-DO's, depending on the difficulty.

Overall, it's a great exercise for me, since I am trying to
learn as much as I can about JS. And I think that is possible
due to focused practice.

Here's a link to the library I'll be using: https://date-fns.org/
Here's the project I am getting ideas from: https://www.theodinproject.com/courses/javascript/lessons/todo-list
# FEATURES WILL INCLUDE:
-description of the current task

-adding dates and due dates

-changing priority of each task

-switching between projects
