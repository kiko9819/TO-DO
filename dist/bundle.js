/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return modal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return showModal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return closeModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__formContentBuilder_formBuilder__ = __webpack_require__(3);

function modal(){
  const wrapper=document.querySelector('#wrapper');
  const mainContent=document.querySelector('.main-content');
  const taskModal=document.createElement('div');
  const taskModalContent=document.createElement('div');

  taskModalContent.className+='taskModal-content';
  taskModal.className+='modal';
  taskModalContent.appendChild(Object(__WEBPACK_IMPORTED_MODULE_0__formContentBuilder_formBuilder__["a" /* default */])());
  taskModal.appendChild(taskModalContent);
  wrapper.appendChild(taskModal);

  return wrapper;
}
//rename to a form builder after its content is separated in
//different modules
function showModal(){
  const hiddenModal=document.querySelector('.modal');
  hiddenModal.style.display='flex';
  return hiddenModal;
}
function closeModal(){
  document.querySelector('.modal').style.display='none';
}



/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(2);
module.exports = __webpack_require__(19);


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__newTaskModal__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__cloneLink_renderProjects__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__newProjectAccordion_buildAccordion__ = __webpack_require__(15);




Object(__WEBPACK_IMPORTED_MODULE_0__newTaskModal__["b" /* modal */])();
const dropDown=document.querySelector('#viewProjectDropDown');
let projects=[];

let expanded=false;
window.onload=function(){
  Object.keys(localStorage).forEach(function(elem){
    console.log(elem);
    Object(__WEBPACK_IMPORTED_MODULE_1__cloneLink_renderProjects__["a" /* renderProjects */])(elem);
  });
}

const Project=(name)=>{
  let tasks=['default'];
  //will listen for click event
  //which will trigger this function below;
  const appendTask=(newTask)=>{
    tasks.push(newTask);
  };
  //will listen for click event
  //which will remove the selected task
  //from the current project
  const removeTask=(position)=>{
    tasks.splice(position,1);
  };

  return {name
    ,appendTask
    ,removeTask
    ,tasks
  };
}
//the default project
const defaultProject=Project(document.querySelector('#default').text);
//used JSON.stringify because setItem accepts only strings and the object
//is not one
localStorage.setItem(defaultProject.name,JSON.stringify(defaultProject));

const listeners=(()=>{
  const newTaskBtn=document.querySelector('#new-to-do-btn');
  const newProjectDropDown=document.querySelector('#changeBtn');
  const newProjectBtn=document.querySelector('.newProjectBtn');
  const defaultMinusBtn=document.querySelector('.minus-background');

  defaultMinusBtn.addEventListener('click',function(){
    this.parentNode.remove();
  });
  newProjectDropDown.addEventListener('click',function(){
    dropDown.classList.toggle('showDropDown');
  });
  newProjectBtn.addEventListener('click',function(){
    newProjectBtn.disabled='true';
    newProjectModal();
  });
  newTaskBtn.addEventListener('click',function(){
    const contentName=document.querySelector('#content-name');
    if(contentName.textContent!=="Choose a project"){
      document.body.insertBefore(Object(__WEBPACK_IMPORTED_MODULE_0__newTaskModal__["c" /* showModal */])(),document.body.firstChild);
    }
    else {
      alert('Choose a project');
    }
  });

})();
function instantiateProject(){
  const createBtn=document.querySelector('.createProjectBtn');
  const addBtn=document.querySelector('.addBtn');
  createBtn.addEventListener('click',function(){
    let projectName=document.querySelector('#projectName');
    const newProject=Project(projectName.value);
    projects.push(newProject);
    localStorage.setItem(newProject.name,JSON.stringify(newProject));
    console.log(JSON.parse(localStorage.getItem(newProject.tasks[0])));
    Object(__WEBPACK_IMPORTED_MODULE_1__cloneLink_renderProjects__["a" /* renderProjects */])(projectName.value);
    projectName.value="";
  });
}

function newProjectModal(){
  if(!expanded){
    dropDown.appendChild(Object(__WEBPACK_IMPORTED_MODULE_2__newProjectAccordion_buildAccordion__["a" /* default */])());
    expanded=!expanded;
    instantiateProject();
    //when the user clicks outside of the menu
    // window.onclick=function(event){
    //   if(!event.target.matches('#viewProjectDropDown'&&dropDown.classList.contains('showDropDown'))){
    //     dropDown.classList.toggle('showDropDown');
    //   }
    // }
  }
  //Here this should prevent 'this' from being undefined
  else if(this!==undefined){
    dropDown.remove(this.firstChild);
    expanded=!expanded;
  }
  else{
    dropDown.appendChild(Object(__WEBPACK_IMPORTED_MODULE_2__newProjectAccordion_buildAccordion__["a" /* default */])());
    expanded=!expanded;
  }
}


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modalBtns__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__labelBuilder__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dateBuilder__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__titleBuilder__ = __webpack_require__(12);




function formBuilder(){
  const form=document.createElement('form');
  form.setAttribute('id','formContent');
  form.style.width='90%';
  form.style.height='80%';
  form.style.display='flex';
  form.style.justifyContent='center';
  form.style.alignItems='center';
  form.style.flexDirection='column';

  form.appendChild(Object(__WEBPACK_IMPORTED_MODULE_1__labelBuilder__["a" /* default */])());
  form.appendChild(Object(__WEBPACK_IMPORTED_MODULE_3__titleBuilder__["a" /* default */])());
  form.appendChild(Object(__WEBPACK_IMPORTED_MODULE_2__dateBuilder__["a" /* default */])());
  form.appendChild(Object(__WEBPACK_IMPORTED_MODULE_0__modalBtns__["b" /* addBtn */])());
  form.appendChild(Object(__WEBPACK_IMPORTED_MODULE_0__modalBtns__["a" /* abortBtn */])());
  return form;
}
/* harmony default export */ __webpack_exports__["a"] = (formBuilder);


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return addBtn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return abortBtn; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__buildNewTask__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__newTaskModal__ = __webpack_require__(0);



function addBtn(){
  const addBtn=document.createElement('button');
  addBtn.setAttribute('class','addBtn');
  addBtn.textContent='ADD TO TASKS';
  addBtn.addEventListener('click',function(event){
    const projectName=document.querySelector('#content-name').textContent;

    event.preventDefault();
    if(validateModal()){
      document.querySelector('.main-content').appendChild(Object(__WEBPACK_IMPORTED_MODULE_0__buildNewTask__["a" /* default */])());
      console.log(projectName);
      Object(__WEBPACK_IMPORTED_MODULE_1__newTaskModal__["a" /* closeModal */])();
    }
  });
  return addBtn;
}
function abortBtn(){
  const abortBtn=document.createElement('button');
  abortBtn.setAttribute('class','abortBtn');
  abortBtn.textContent='ABORT';
  abortBtn.addEventListener('click',function(event){
    event.preventDefault();
    Object(__WEBPACK_IMPORTED_MODULE_1__newTaskModal__["a" /* closeModal */])();
  });
  return abortBtn;
}
function validateModal(){
  const date=document.querySelector('input[type="date"]').value;
  const title=document.querySelector('input[type="text"]').value;
  if(date==''||title==''){
    return false;
  }else{
    return true;
  }
}




/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__taskBuilder_expandBtn__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__taskBuilder_panel__ = __webpack_require__(7);



function buildNewTask(){
  const taskContainer=document.createElement('div');
  const taskName=document.createElement('label');
  const dueDate=document.createElement('label');
  const labelsContainer=document.createElement('div');
  const expandableBlock=document.createElement('div');

  labelsContainer.style.display="flex";
  labelsContainer.style.justifyContent="space-between";
  labelsContainer.style.flexDirection="row";
  labelsContainer.style.fontSize="70%";
  labelsContainer.style.width="95%";
  labelsContainer.style.height="20%";
  labelsContainer.style.borderBottom="1px solid rgba(0,0,0,0.3)";
  labelsContainer.style.marginBottom=".5em";

  dueDate.textContent="Due to:"+document.querySelector('input[type="date"]').value;
  taskName.textContent="Task: "+document.querySelector('input[type="text"]').value;

  taskContainer.style.display="flex";
  taskContainer.style.width="100%";
  taskContainer.style.height="auto";
  taskContainer.style.justifyContent="flex-start";
  taskContainer.style.alignItems="center";
  taskContainer.style.flexDirection="column";
  taskContainer.setAttribute('id','taskContainer');

  expandableBlock.width="100%";
  expandableBlock.style.display="block";

  labelsContainer.appendChild(taskName);
  labelsContainer.appendChild(dueDate);
  taskContainer.appendChild(labelsContainer);
  taskContainer.appendChild(Object(__WEBPACK_IMPORTED_MODULE_1__taskBuilder_panel__["a" /* default */])());

  taskContainer.appendChild(Object(__WEBPACK_IMPORTED_MODULE_0__taskBuilder_expandBtn__["a" /* default */])());
  expandableBlock.appendChild(taskContainer);
  return taskContainer;
}


/* harmony default export */ __webpack_exports__["a"] = (buildNewTask);


/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//changes the direction of the arrow
function expandBtn(){
  const expandBtn=document.createElement('button');

  expandBtn.className+="expandBtn";
  expandBtn.className+=" fa fa-caret-down";
  expandBtn.setAttribute('id','expansionBtn');

  expandBtn.addEventListener('click',function(){
    let panel=this.previousElementSibling;
    let expandBtnClass=this.classList;

    if(panel.style.display==='flex'){
      panel.style.display='none';
      if(expandBtnClass.contains('fa-caret-down')){
        expandBtnClass.remove('fa-caret-up');
      }
      else{
        expandBtnClass.add('fa-caret-down');
      }
      if(expandBtnClass.contains('fa-caret-up')){
        expandBtnClass.remove('fa-caret-up');
      }
      else{
        expandBtnClass.add('fa-caret-down');
      }
    }
    else{
      panel.style.display='flex';
      expandBtnClass.remove('fa-caret-down');
      expandBtnClass.add('fa-caret-up');
    }
  });
  return expandBtn;
}
/* harmony default export */ __webpack_exports__["a"] = (expandBtn);


/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__actionBtns__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__priorityBtns__ = __webpack_require__(9);


//creates the priorities, side notes and appends them to a panel
//along with the actionBtns;
function makePanel(){
  const panel=document.createElement('div');
  const textArea=document.createElement('TEXTAREA');
  const priorityDiv=document.createElement('div');
  const priorityLabel=document.createElement('label');
  const btnsDiv=document.createElement('div');

  priorityDiv.style.display='flex';
  priorityDiv.style.flexDirection='row';
  priorityDiv.style.justifyContent='space-around';
  priorityDiv.style.width='100%';
  priorityDiv.style.height='auto';

  priorityLabel.innerHTML='Priority: ';
  priorityLabel.style.display='flex';
  priorityLabel.style.flexDirection='row';

  priorityLabel.style.width='100%'
  priorityLabel.style.justifyContent='center';
  priorityLabel.appendChild(Object(__WEBPACK_IMPORTED_MODULE_1__priorityBtns__["a" /* default */])());
  priorityDiv.appendChild(priorityLabel);

  textArea.style.width='auto';
  textArea.placeholder='Side notes';
  textArea.style.border='4px dotted rgba(0,0,0,0.3)';

  panel.style.width='100%';
  panel.style.display='none';
  panel.style.marginBottom='.5em';
  panel.style.flexDirection='column';
  panel.appendChild(textArea);
  panel.appendChild(priorityDiv);
  panel.appendChild(Object(__WEBPACK_IMPORTED_MODULE_0__actionBtns__["a" /* default */])());
  return panel;
}
/* harmony default export */ __webpack_exports__["a"] = (makePanel);


/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function actionBtns(){
  const btnsDiv=document.createElement('div');
  const deleteBtn=document.createElement('button');
  deleteBtn.className+='delBtn';
  deleteBtn.textContent='Delete task';

  btnsDiv.style.display='flex';
  btnsDiv.style.flexDirection='row';
  btnsDiv.style.width='100%';
  btnsDiv.style.height='auto';
  btnsDiv.style.justifyContent='center';

  deleteBtn.addEventListener('click',function(){
    const taskContainerParent=document.getElementById('taskContainer');
    taskContainerParent.remove();
  })
  btnsDiv.appendChild(deleteBtn);

  return btnsDiv;
}
/* harmony default export */ __webpack_exports__["a"] = (actionBtns);


/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function priorityBtns(){
  const high=document.createElement('button');
  const medium=document.createElement('button');
  const low=document.createElement('button');
  const container=document.createElement('div');

  high.style.backgroundColor='red';
  high.style.width='2em';
  high.style.height='2em';
  high.style.border='none';

  medium.style.backgroundColor='orange';
  medium.style.width='2em';
  medium.style.height='2em';
  medium.style.border='none';


  low.style.backgroundColor='green';
  low.style.width='2em';
  low.style.height='2em';
  low.style.border='none';

  container.style.display='flex';
  container.style.alignItems='center';
  container.appendChild(high);
  container.appendChild(medium);
  container.appendChild(low);
  return container;
}
/* harmony default export */ __webpack_exports__["a"] = (priorityBtns);


/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function label(){
  const label=document.createElement('label');
  label.innerHTML='New Task';
  label.fontSize='150%';
  label.width='100%';
  return label;
}
/* harmony default export */ __webpack_exports__["a"] = (label);


/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function date(){
  const date=document.createElement('input');
  date.setAttribute('type','date');
  date.setAttribute('required','');
  date.style.border='none';
  date.style.borderLeft='5px solid rgb(128,128,255)';
  date.style.width='100%';
  date.style.padding='2%';
  date.style.marginBottom='5%';
  date.style.height='4em';
  return date;
}
/* harmony default export */ __webpack_exports__["a"] = (date);


/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function title(){
  const title=document.createElement('input');
  title.setAttribute('type','text');
  title.setAttribute('required','');
  title.style.border='none';
  title.style.borderLeft='5px solid rgb(128,128,255)';
  title.placeholder='Title goes here';
  title.style.width='100%';
  title.style.padding='2%';
  title.style.marginBottom='5%';
  title.style.height='4em';
  return title;
}
/* harmony default export */ __webpack_exports__["a"] = (title);


/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return renderProjects; });
/* unused harmony export uniqueProjects */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__clonerCreator__ = __webpack_require__(14);

let myProjects=[];
let uniqueProjects;
//renders the myProjects array
//and filters it for repeating elements
//then appends the non-repeating ones
function renderProjects(projectName){
  const projectList=document.querySelector('#list-of-items');

  myProjects.push(projectName);
  uniqueProjects=myProjects.filter(function(elem,i){
    return myProjects.indexOf(elem)===i;
  });

  projectList.innerHTML='';
  uniqueProjects.forEach(function(element){
    if(element!==""){
      projectList.appendChild(Object(__WEBPACK_IMPORTED_MODULE_0__clonerCreator__["a" /* default */])(element));
    }
  });

  //filters both of the arrays myProjects and uniqueProjects
  //when trying to delete an element
  //this prevents the drop-down from repeating old,deleted elements
  //when a new one is created
  const delBtn=document.querySelectorAll('#deleteBtn');
  Array.from(delBtn).forEach(function(elem){
    elem.addEventListener('click',function(){
      uniqueProjects=uniqueProjects.filter(e=>e!==elem.previousSibling.text);
      myProjects=myProjects.filter(e=>e!==elem.previousSibling.text);
      localStorage.removeItem(this.previousSibling.text);
      document.querySelector('#content-name').textContent="Choose a project"
      console.log(uniqueProjects);
      console.log(myProjects);
    });
  });
}




/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function cloneCreator(newProject){
  const container=document.createElement('div');
  const link=document.createElement('a');
  const btn=document.createElement('button');

  container.className+='list-item';
  link.textContent=newProject;
  link.setAttribute('href','#');
  btn.classList.add('minus-background','fa','fa-minus-square');
  btn.setAttribute('aria-hidden','true');
  btn.setAttribute('id','deleteBtn');
  btn.addEventListener('click',function(){
    container.remove();
  });
  link.addEventListener('click',function(link){
    const mainContent=document.querySelector('.main-content');
    const contentName=document.querySelector('#content-name');

    contentName.textContent=link.target.textContent;
    container.classList.toggle('projectColor');
    const project=JSON.parse(localStorage.getItem(contentName.textContent));
    if(mainContent.childElementCount!==1){
      console.log('clearing...');
      console.log('loading this projects tasks...')
      //renderTasks();
    }
  });

  container.appendChild(link);
  container.appendChild(btn);

  return container;
}
// function renderTasks(){
//   const currentProjectName=document.querySelector('#content-name').textContent;
//
// }
/* harmony default export */ __webpack_exports__["a"] = (cloneCreator);


/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__heading__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__textField__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__newProjectBtn__ = __webpack_require__(18);




function buildAccordion(){
  const accordionContent=document.createElement('div');

  accordionContent.style.display='flex';
  accordionContent.style.flexDirection='column';
  accordionContent.style.justifyContent='center';
  accordionContent.zIndex='1';
  accordionContent.setAttribute('id','accordionContent');

  accordionContent.appendChild(Object(__WEBPACK_IMPORTED_MODULE_0__heading__["a" /* default */])());
  accordionContent.appendChild(Object(__WEBPACK_IMPORTED_MODULE_1__textField__["a" /* default */])());
  accordionContent.appendChild(Object(__WEBPACK_IMPORTED_MODULE_2__newProjectBtn__["a" /* default */])());

  return accordionContent;
}
/* harmony default export */ __webpack_exports__["a"] = (buildAccordion);


/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function headLineLabel(){
  const headLine=document.createElement('label');

  headLine.style.width='100%';
  headLine.style.height='2em';
  headLine.style.fontSize='90%';
  headLine.textContent='New project name';
  headLine.style.textAlign='center';
  return headLine;
}
/* harmony default export */ __webpack_exports__["a"] = (headLineLabel);


/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function projectName(){
  const textField=document.createElement('input');
  textField.type='text';

  textField.style.width='40%';
  textField.style.alignSelf='center';
  textField.style.marginBottom='1rem';
  textField.setAttribute('id','projectName');
  return textField;
}
/* harmony default export */ __webpack_exports__["a"] = (projectName);


/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function projectDropDownBtns(){
  const createProjectBtn=document.createElement('button');
  const closeCreationBtn=document.createElement('button');
  const btnContainer=document.createElement('div');

  btnContainer.style.display='flex';
  btnContainer.style.flexDirection='row';
  btnContainer.style.width='100%';
  btnContainer.style.height='auto';

  createProjectBtn.className+='createProjectBtn';
  createProjectBtn.textContent='Create';
  closeCreationBtn.className+='closeCreationBtn';
  closeCreationBtn.textContent='Cancel';

  closeCreationBtn.addEventListener('click',function(){
    document.querySelector('.newProjectBtn').disabled=false;
    document.querySelector('#accordionContent').remove();
  });
  btnContainer.appendChild(createProjectBtn);
  btnContainer.appendChild(closeCreationBtn);

  return btnContainer;
}

/* harmony default export */ __webpack_exports__["a"] = (projectDropDownBtns);


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__dirname) {const path=__webpack_require__(20);
module.exports={
	entry: './src/index.js',
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname,'dist')
	},
	watch:true
};

/* WEBPACK VAR INJECTION */}.call(exports, "/"))

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

// resolves . and .. elements in a path array with directory names there
// must be no slashes, empty elements, or device names (c:\) in the array
// (so also no leading and trailing slashes - it does not distinguish
// relative and absolute paths)
function normalizeArray(parts, allowAboveRoot) {
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = parts.length - 1; i >= 0; i--) {
    var last = parts[i];
    if (last === '.') {
      parts.splice(i, 1);
    } else if (last === '..') {
      parts.splice(i, 1);
      up++;
    } else if (up) {
      parts.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (allowAboveRoot) {
    for (; up--; up) {
      parts.unshift('..');
    }
  }

  return parts;
}

// Split a filename into [root, dir, basename, ext], unix version
// 'root' is just a slash, or nothing.
var splitPathRe =
    /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
var splitPath = function(filename) {
  return splitPathRe.exec(filename).slice(1);
};

// path.resolve([from ...], to)
// posix version
exports.resolve = function() {
  var resolvedPath = '',
      resolvedAbsolute = false;

  for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
    var path = (i >= 0) ? arguments[i] : process.cwd();

    // Skip empty and invalid entries
    if (typeof path !== 'string') {
      throw new TypeError('Arguments to path.resolve must be strings');
    } else if (!path) {
      continue;
    }

    resolvedPath = path + '/' + resolvedPath;
    resolvedAbsolute = path.charAt(0) === '/';
  }

  // At this point the path should be resolved to a full absolute path, but
  // handle relative paths to be safe (might happen when process.cwd() fails)

  // Normalize the path
  resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {
    return !!p;
  }), !resolvedAbsolute).join('/');

  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
};

// path.normalize(path)
// posix version
exports.normalize = function(path) {
  var isAbsolute = exports.isAbsolute(path),
      trailingSlash = substr(path, -1) === '/';

  // Normalize the path
  path = normalizeArray(filter(path.split('/'), function(p) {
    return !!p;
  }), !isAbsolute).join('/');

  if (!path && !isAbsolute) {
    path = '.';
  }
  if (path && trailingSlash) {
    path += '/';
  }

  return (isAbsolute ? '/' : '') + path;
};

// posix version
exports.isAbsolute = function(path) {
  return path.charAt(0) === '/';
};

// posix version
exports.join = function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return exports.normalize(filter(paths, function(p, index) {
    if (typeof p !== 'string') {
      throw new TypeError('Arguments to path.join must be strings');
    }
    return p;
  }).join('/'));
};


// path.relative(from, to)
// posix version
exports.relative = function(from, to) {
  from = exports.resolve(from).substr(1);
  to = exports.resolve(to).substr(1);

  function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
      if (arr[start] !== '') break;
    }

    var end = arr.length - 1;
    for (; end >= 0; end--) {
      if (arr[end] !== '') break;
    }

    if (start > end) return [];
    return arr.slice(start, end - start + 1);
  }

  var fromParts = trim(from.split('/'));
  var toParts = trim(to.split('/'));

  var length = Math.min(fromParts.length, toParts.length);
  var samePartsLength = length;
  for (var i = 0; i < length; i++) {
    if (fromParts[i] !== toParts[i]) {
      samePartsLength = i;
      break;
    }
  }

  var outputParts = [];
  for (var i = samePartsLength; i < fromParts.length; i++) {
    outputParts.push('..');
  }

  outputParts = outputParts.concat(toParts.slice(samePartsLength));

  return outputParts.join('/');
};

exports.sep = '/';
exports.delimiter = ':';

exports.dirname = function(path) {
  var result = splitPath(path),
      root = result[0],
      dir = result[1];

  if (!root && !dir) {
    // No dirname whatsoever
    return '.';
  }

  if (dir) {
    // It has a dirname, strip trailing slash
    dir = dir.substr(0, dir.length - 1);
  }

  return root + dir;
};


exports.basename = function(path, ext) {
  var f = splitPath(path)[2];
  // TODO: make this comparison case-insensitive on windows?
  if (ext && f.substr(-1 * ext.length) === ext) {
    f = f.substr(0, f.length - ext.length);
  }
  return f;
};


exports.extname = function(path) {
  return splitPath(path)[3];
};

function filter (xs, f) {
    if (xs.filter) return xs.filter(f);
    var res = [];
    for (var i = 0; i < xs.length; i++) {
        if (f(xs[i], i, xs)) res.push(xs[i]);
    }
    return res;
}

// String.prototype.substr - negative index don't work in IE8
var substr = 'ab'.substr(-1) === 'b'
    ? function (str, start, len) { return str.substr(start, len) }
    : function (str, start, len) {
        if (start < 0) start = str.length + start;
        return str.substr(start, len);
    }
;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(21)))

/***/ }),
/* 21 */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ })
/******/ ]);