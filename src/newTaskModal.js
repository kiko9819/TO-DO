import formBuilder from './formContentBuilder/formBuilder';

function modal() {
  const wrapper = document.querySelector('#wrapper');
  const mainContent = document.querySelector('.main-content');
  const taskModal = document.createElement('div');
  const taskModalContent = document.createElement('div');

  taskModalContent.className += 'taskModal-content';
  taskModal.className += 'modal';
  taskModalContent.appendChild(formBuilder());
  taskModal.appendChild(taskModalContent);
  wrapper.appendChild(taskModal);

  return wrapper;
}
//rename to a form builder after its content is separated in
//different modules
function showModal() {
  const hiddenModal = document.querySelector('.modal');
  hiddenModal.style.display = 'flex';
  return hiddenModal;
}

function closeModal() {
  document.querySelector('.modal').style.display = 'none';
}
export {
  modal,
  showModal,
  closeModal
};
