import {
  modal,
  showModal
} from './newTaskModal';
import {
  renderProjects,
  uniqueProjects
} from './cloneLink/renderProjects';
import buildAccordion from './newProjectAccordion/buildAccordion';

modal();
const dropDown = document.querySelector('#viewProjectDropDown');
let projects = [];

let expanded = false;
window.onload = function() {
  Object.keys(localStorage).forEach(function(elem) {
    console.log(elem);
    renderProjects(elem);
  });
}

const Project = (name) => {
  let tasks = ['default'];
  //will listen for click event
  //which will trigger this function below;
  const appendTask = (newTask) => {
    tasks.push(newTask);
  };
  //will listen for click event
  //which will remove the selected task
  //from the current project
  const removeTask = (position) => {
    tasks.splice(position, 1);
  };

  return {
    name,
    appendTask,
    removeTask,
    tasks
  };
}
//the default project
const defaultProject = Project(document.querySelector('#default').text);
//used JSON.stringify because setItem accepts only strings and the object
//is not one
localStorage.setItem(defaultProject.name, JSON.stringify(defaultProject));

const listeners = (() => {
  const newTaskBtn = document.querySelector('#new-to-do-btn');
  const newProjectDropDown = document.querySelector('#changeBtn');
  const newProjectBtn = document.querySelector('.newProjectBtn');
  const defaultMinusBtn = document.querySelector('.minus-background');

  defaultMinusBtn.addEventListener('click', function() {
    this.parentNode.remove();
  });
  newProjectDropDown.addEventListener('click', function() {
    dropDown.classList.toggle('showDropDown');
  });
  newProjectBtn.addEventListener('click', function() {
    newProjectBtn.disabled = 'true';
    newProjectModal();
  });
  newTaskBtn.addEventListener('click', function() {
    const contentName = document.querySelector('#content-name');
    if (contentName.textContent !== "Choose a project") {
      document.body.insertBefore(showModal(), document.body.firstChild);
    } else {
      alert('Choose a project');
    }
  });

})();

function instantiateProject() {
  const createBtn = document.querySelector('.createProjectBtn');
  const addBtn = document.querySelector('.addBtn');
  createBtn.addEventListener('click', function() {
    let projectName = document.querySelector('#projectName');
    const newProject = Project(projectName.value);
    projects.push(newProject);
    localStorage.setItem(newProject.name, JSON.stringify(newProject));
    console.log(JSON.parse(localStorage.getItem(newProject.tasks[0])));
    renderProjects(projectName.value);
    projectName.value = "";
  });
}

function newProjectModal() {
  if (!expanded) {
    dropDown.appendChild(buildAccordion());
    expanded = !expanded;
    instantiateProject();
    //when the user clicks outside of the menu
    // window.onclick=function(event){
    //   if(!event.target.matches('#viewProjectDropDown'&&dropDown.classList.contains('showDropDown'))){
    //     dropDown.classList.toggle('showDropDown');
    //   }
    // }
  }
  //Here this should prevent 'this' from being undefined
  else if (this !== undefined) {
    dropDown.remove(this.firstChild);
    expanded = !expanded;
  } else {
    dropDown.appendChild(buildAccordion());
    expanded = !expanded;
  }
}
