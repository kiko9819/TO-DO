import buildNewTask from '../buildNewTask';
import {
  closeModal
} from '../newTaskModal';

function addBtn() {
  const addBtn = document.createElement('button');
  addBtn.setAttribute('class', 'addBtn');
  addBtn.textContent = 'ADD TO TASKS';
  addBtn.addEventListener('click', function(event) {
    const projectName = document.querySelector('#content-name').textContent;

    event.preventDefault();
    if (validateModal()) {
      document.querySelector('.main-content').appendChild(buildNewTask());
      console.log(projectName);
      closeModal();
    }
  });
  return addBtn;
}

function abortBtn() {
  const abortBtn = document.createElement('button');
  abortBtn.setAttribute('class', 'abortBtn');
  abortBtn.textContent = 'ABORT';
  abortBtn.addEventListener('click', function(event) {
    event.preventDefault();
    closeModal();
  });
  return abortBtn;
}

function validateModal() {
  const date = document.querySelector('input[type="date"]').value;
  const title = document.querySelector('input[type="text"]').value;
  if (date == '' || title == '') {
    return false;
  } else {
    return true;
  }
}

export {
  addBtn,
  abortBtn
};
