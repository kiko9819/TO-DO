import {
  addBtn,
  abortBtn
} from './modalBtns';
import label from './labelBuilder';
import date from './dateBuilder';
import title from './titleBuilder';

function formBuilder() {
  const form = document.createElement('form');
  form.setAttribute('id', 'formContent');
  form.style.width = '90%';
  form.style.height = '80%';
  form.style.display = 'flex';
  form.style.justifyContent = 'center';
  form.style.alignItems = 'center';
  form.style.flexDirection = 'column';

  form.appendChild(label());
  form.appendChild(title());
  form.appendChild(date());
  form.appendChild(addBtn());
  form.appendChild(abortBtn());
  return form;
}
export default formBuilder;
