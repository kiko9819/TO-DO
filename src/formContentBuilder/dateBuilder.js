function date() {
  const date = document.createElement('input');
  date.setAttribute('type', 'date');
  date.setAttribute('required', '');
  date.style.border = 'none';
  date.style.borderLeft = '5px solid rgb(128,128,255)';
  date.style.width = '100%';
  date.style.padding = '2%';
  date.style.marginBottom = '5%';
  date.style.height = '4em';
  return date;
}
export default date;
