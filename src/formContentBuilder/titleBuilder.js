function title() {
  const title = document.createElement('input');
  title.setAttribute('type', 'text');
  title.setAttribute('required', '');
  title.style.border = 'none';
  title.style.borderLeft = '5px solid rgb(128,128,255)';
  title.placeholder = 'Title goes here';
  title.style.width = '100%';
  title.style.padding = '2%';
  title.style.marginBottom = '5%';
  title.style.height = '4em';
  return title;
}
export default title;
