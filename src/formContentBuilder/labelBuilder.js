function label() {
  const label = document.createElement('label');
  label.innerHTML = 'New Task';
  label.fontSize = '150%';
  label.width = '100%';
  return label;
}
export default label;
