function headLineLabel() {
  const headLine = document.createElement('label');

  headLine.style.width = '100%';
  headLine.style.height = '2em';
  headLine.style.fontSize = '90%';
  headLine.textContent = 'New project name';
  headLine.style.textAlign = 'center';
  return headLine;
}
export default headLineLabel;
