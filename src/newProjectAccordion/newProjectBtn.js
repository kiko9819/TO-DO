function projectDropDownBtns() {
  const createProjectBtn = document.createElement('button');
  const closeCreationBtn = document.createElement('button');
  const btnContainer = document.createElement('div');

  btnContainer.style.display = 'flex';
  btnContainer.style.flexDirection = 'row';
  btnContainer.style.width = '100%';
  btnContainer.style.height = 'auto';

  createProjectBtn.className += 'createProjectBtn';
  createProjectBtn.textContent = 'Create';
  closeCreationBtn.className += 'closeCreationBtn';
  closeCreationBtn.textContent = 'Cancel';

  closeCreationBtn.addEventListener('click', function() {
    document.querySelector('.newProjectBtn').disabled = false;
    document.querySelector('#accordionContent').remove();
  });
  btnContainer.appendChild(createProjectBtn);
  btnContainer.appendChild(closeCreationBtn);

  return btnContainer;
}

export default projectDropDownBtns;
