function projectName() {
  const textField = document.createElement('input');
  textField.type = 'text';

  textField.style.width = '40%';
  textField.style.alignSelf = 'center';
  textField.style.marginBottom = '1rem';
  textField.setAttribute('id', 'projectName');
  return textField;
}
export default projectName;
