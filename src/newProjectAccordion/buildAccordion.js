import headLineLabel from './heading';
import projectName from './textField';
import projectDropDownBtns from './newProjectBtn';

function buildAccordion() {
  const accordionContent = document.createElement('div');

  accordionContent.style.display = 'flex';
  accordionContent.style.flexDirection = 'column';
  accordionContent.style.justifyContent = 'center';
  accordionContent.zIndex = '1';
  accordionContent.setAttribute('id', 'accordionContent');

  accordionContent.appendChild(headLineLabel());
  accordionContent.appendChild(projectName());
  accordionContent.appendChild(projectDropDownBtns());

  return accordionContent;
}
export default buildAccordion;
