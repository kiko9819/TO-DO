function priorityBtns() {
  const high = document.createElement('button');
  const medium = document.createElement('button');
  const low = document.createElement('button');
  const container = document.createElement('div');

  high.style.backgroundColor = 'red';
  high.style.width = '2em';
  high.style.height = '2em';
  high.style.border = 'none';

  medium.style.backgroundColor = 'orange';
  medium.style.width = '2em';
  medium.style.height = '2em';
  medium.style.border = 'none';


  low.style.backgroundColor = 'green';
  low.style.width = '2em';
  low.style.height = '2em';
  low.style.border = 'none';

  container.style.display = 'flex';
  container.style.alignItems = 'center';
  container.appendChild(high);
  container.appendChild(medium);
  container.appendChild(low);
  return container;
}
export default priorityBtns;
