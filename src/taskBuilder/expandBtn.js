//changes the direction of the arrow
function expandBtn() {
  const expandBtn = document.createElement('button');

  expandBtn.className += "expandBtn";
  expandBtn.className += " fa fa-caret-down";
  expandBtn.setAttribute('id', 'expansionBtn');

  expandBtn.addEventListener('click', function() {
    let panel = this.previousElementSibling;
    let expandBtnClass = this.classList;

    if (panel.style.display === 'flex') {
      panel.style.display = 'none';
      if (expandBtnClass.contains('fa-caret-down')) {
        expandBtnClass.remove('fa-caret-up');
      } else {
        expandBtnClass.add('fa-caret-down');
      }
      if (expandBtnClass.contains('fa-caret-up')) {
        expandBtnClass.remove('fa-caret-up');
      } else {
        expandBtnClass.add('fa-caret-down');
      }
    } else {
      panel.style.display = 'flex';
      expandBtnClass.remove('fa-caret-down');
      expandBtnClass.add('fa-caret-up');
    }
  });
  return expandBtn;
}
export default expandBtn;
