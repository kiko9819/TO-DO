import actionBtns from './actionBtns';
import priorityBtns from './priorityBtns';
//creates the priorities, side notes and appends them to a panel
//along with the actionBtns;
function makePanel() {
  const panel = document.createElement('div');
  const textArea = document.createElement('TEXTAREA');
  const priorityDiv = document.createElement('div');
  const priorityLabel = document.createElement('label');
  const btnsDiv = document.createElement('div');

  priorityDiv.style.display = 'flex';
  priorityDiv.style.flexDirection = 'row';
  priorityDiv.style.justifyContent = 'space-around';
  priorityDiv.style.width = '100%';
  priorityDiv.style.height = 'auto';

  priorityLabel.innerHTML = 'Priority: ';
  priorityLabel.style.display = 'flex';
  priorityLabel.style.flexDirection = 'row';

  priorityLabel.style.width = '100%'
  priorityLabel.style.justifyContent = 'center';
  priorityLabel.appendChild(priorityBtns());
  priorityDiv.appendChild(priorityLabel);

  textArea.style.width = 'auto';
  textArea.placeholder = 'Side notes';
  textArea.style.border = '4px dotted rgba(0,0,0,0.3)';

  panel.style.width = '100%';
  panel.style.display = 'none';
  panel.style.marginBottom = '.5em';
  panel.style.flexDirection = 'column';
  panel.appendChild(textArea);
  panel.appendChild(priorityDiv);
  panel.appendChild(actionBtns());
  return panel;
}
export default makePanel;
