function actionBtns() {
  const btnsDiv = document.createElement('div');
  const deleteBtn = document.createElement('button');
  deleteBtn.className += 'delBtn';
  deleteBtn.textContent = 'Delete task';

  btnsDiv.style.display = 'flex';
  btnsDiv.style.flexDirection = 'row';
  btnsDiv.style.width = '100%';
  btnsDiv.style.height = 'auto';
  btnsDiv.style.justifyContent = 'center';

  deleteBtn.addEventListener('click', function() {
    const taskContainerParent = document.getElementById('taskContainer');
    taskContainerParent.remove();
  })
  btnsDiv.appendChild(deleteBtn);

  return btnsDiv;
}
export default actionBtns;
