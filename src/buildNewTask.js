import expandBtn from './taskBuilder/expandBtn';
import makePanel from './taskBuilder/panel';

function buildNewTask() {
  const taskContainer = document.createElement('div');
  const taskName = document.createElement('label');
  const dueDate = document.createElement('label');
  const labelsContainer = document.createElement('div');
  const expandableBlock = document.createElement('div');

  labelsContainer.style.display = "flex";
  labelsContainer.style.justifyContent = "space-between";
  labelsContainer.style.flexDirection = "row";
  labelsContainer.style.fontSize = "70%";
  labelsContainer.style.width = "95%";
  labelsContainer.style.height = "20%";
  labelsContainer.style.borderBottom = "1px solid rgba(0,0,0,0.3)";
  labelsContainer.style.marginBottom = ".5em";

  dueDate.textContent = "Due to:" + document.querySelector('input[type="date"]').value;
  taskName.textContent = "Task: " + document.querySelector('input[type="text"]').value;

  taskContainer.style.display = "flex";
  taskContainer.style.width = "100%";
  taskContainer.style.height = "auto";
  taskContainer.style.justifyContent = "flex-start";
  taskContainer.style.alignItems = "center";
  taskContainer.style.flexDirection = "column";
  taskContainer.setAttribute('id', 'taskContainer');

  expandableBlock.width = "100%";
  expandableBlock.style.display = "block";

  labelsContainer.appendChild(taskName);
  labelsContainer.appendChild(dueDate);
  taskContainer.appendChild(labelsContainer);
  taskContainer.appendChild(makePanel());

  taskContainer.appendChild(expandBtn());
  expandableBlock.appendChild(taskContainer);
  return taskContainer;
}


export default buildNewTask;
