import clonerCreator from './clonerCreator';
let myProjects = [];
let uniqueProjects;
//renders the myProjects array
//and filters it for repeating elements
//then appends the non-repeating ones
function renderProjects(projectName) {
  const projectList = document.querySelector('#list-of-items');

  myProjects.push(projectName);
  uniqueProjects = myProjects.filter(function(elem, i) {
    return myProjects.indexOf(elem) === i;
  });

  projectList.innerHTML = '';
  uniqueProjects.forEach(function(element) {
    if (element !== "") {
      projectList.appendChild(clonerCreator(element));
    }
  });

  //filters both of the arrays myProjects and uniqueProjects
  //when trying to delete an element
  //this prevents the drop-down from repeating old,deleted elements
  //when a new one is created
  const delBtn = document.querySelectorAll('#deleteBtn');
  Array.from(delBtn).forEach(function(elem) {
    elem.addEventListener('click', function() {
      uniqueProjects = uniqueProjects.filter(e => e !== elem.previousSibling.text);
      myProjects = myProjects.filter(e => e !== elem.previousSibling.text);
      localStorage.removeItem(this.previousSibling.text);
      document.querySelector('#content-name').textContent = "Choose a project"
      console.log(uniqueProjects);
      console.log(myProjects);
    });
  });
}

export {
  renderProjects,
  uniqueProjects
};
