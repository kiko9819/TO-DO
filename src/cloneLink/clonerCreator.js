function cloneCreator(newProject) {
  const container = document.createElement('div');
  const link = document.createElement('a');
  const btn = document.createElement('button');

  container.className += 'list-item';
  link.textContent = newProject;
  link.setAttribute('href', '#');
  btn.classList.add('minus-background', 'fa', 'fa-minus-square');
  btn.setAttribute('aria-hidden', 'true');
  btn.setAttribute('id', 'deleteBtn');
  btn.addEventListener('click', function() {
    container.remove();
  });
  link.addEventListener('click', function(link) {
    const mainContent = document.querySelector('.main-content');
    const contentName = document.querySelector('#content-name');

    contentName.textContent = link.target.textContent;
    container.classList.toggle('projectColor');
    const project = JSON.parse(localStorage.getItem(contentName.textContent));
    if (mainContent.childElementCount !== 1) {
      console.log('clearing...');
      console.log('loading this projects tasks...')
      //renderTasks();
    }
  });

  container.appendChild(link);
  container.appendChild(btn);

  return container;
}
// function renderTasks(){
//   const currentProjectName=document.querySelector('#content-name').textContent;
//
// }
export default cloneCreator;
